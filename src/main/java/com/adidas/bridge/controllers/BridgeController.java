package com.adidas.bridge.controllers;

import com.adidas.bridge.dtos.requests.SubscriptionRequest;
import com.adidas.bridge.dtos.responses.ApiResponseDTO;
import com.adidas.bridge.dtos.responses.SubscriptionData;
import com.adidas.bridge.exceptions.SubscriptionRequestException;
import com.adidas.bridge.services.SubscriptionService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.util.CollectionUtils.isEmpty;

@RestController
public class BridgeController {

	@Autowired
	private SubscriptionService subscriptionService;

	@ApiOperation(
			value = "Create Subscription to newsletter campaign",
			notes = "Creates a subcription and returns an ID",
			tags = {"Subscription"})
	@ApiResponses(
			value = {
					@ApiResponse(code = 201, message = "Subscription Created. Returns ID of the newly created subscription", response = ApiResponseDTO.class),
					@ApiResponse(
							code = 400,
							message = "Returns ApiResponseDTO object with cause details",
							response = ApiResponseDTO.class)
			})
	@PostMapping(value = "/v1.0/subscriptions",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponseDTO<?>> createSubscription(@RequestBody @Valid SubscriptionRequest subscriptionRequest) {
		ApiResponseDTO<?> response;
		try {
			Long subscriptionId = subscriptionService.createSubscription(subscriptionRequest);
			response = new ApiResponseDTO<>(HttpStatus.CREATED, subscriptionId);

			return ResponseEntity.status(response.getStatus()).body(response);
		} catch (SubscriptionRequestException e) {
			response = new ApiResponseDTO<Object>(HttpStatus.BAD_REQUEST, e.getLocalizedMessage(), e.getErrorDetails());
			return ResponseEntity.status(response.getStatus()).body(response);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

	}

	@ApiOperation(
			value = "Retrieve Subscriptions list",
			notes = "Retrieve all subscriptions or search subscriptions by params",
			tags = {"Subscription"})
	@ApiResponses(
			value = {
					@ApiResponse(code = 200, message = "successful operation", response = ApiResponseDTO.class),
					@ApiResponse(
							code = 400,
							message = "Returns ApiResponseDTO object with cause details",
							response = ApiResponseDTO.class)
			})
	@GetMapping(value = "/v1.0/subscriptions", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponseDTO<List<SubscriptionData>>> retrieveSubscriptions(
			@RequestParam(required = false) List<Long> ids,
			@RequestParam(required = false) List<String> emails,
			@RequestParam(required = false) List<Long> newsletterIds
	) {
		ApiResponseDTO<List<SubscriptionData>> response;
		try {
			List<SubscriptionData> subscriptionDataList = subscriptionService.retrieveSubscriptions(ids, emails, newsletterIds);
			response = new ApiResponseDTO<>(HttpStatus.OK, subscriptionDataList);

			if(isEmpty(subscriptionDataList)) {
				response = new ApiResponseDTO<>(HttpStatus.NO_CONTENT, subscriptionDataList);
			}

			return ResponseEntity.status(response.getStatus()).body(response);
		} catch (SubscriptionRequestException exception) {
			response = new ApiResponseDTO<>(HttpStatus.BAD_REQUEST, exception);
			return ResponseEntity.status(response.getStatus()).body(response);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}


	@ApiOperation(
			value = "Retrieve Subscription by ID",
			notes = "Retrieves Subscription details of a given Subscription ID",
			tags = {"Subscription"})
	@ApiResponses(
			value = {
					@ApiResponse(code = 200, message = "Returns ApiResponseDTO object with the resource details", response = ApiResponseDTO.class),
					@ApiResponse(
							code = 404,
							message = "Returns ApiResponseDTO object with cause details, mainly the given Subscription ID is not associated with any known resource.",
							response = ApiResponseDTO.class)
			})
	@GetMapping(value = "/v1.0/subscriptions/{subscriptionId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponseDTO<SubscriptionData>> retrieveSubscriptionById(@PathVariable Long subscriptionId) {
		ApiResponseDTO<SubscriptionData> response;
		try {
			SubscriptionData subscriptionData = subscriptionService.findSubscription(subscriptionId);
			response = new ApiResponseDTO<>(HttpStatus.OK, subscriptionData);

			return ResponseEntity.status(response.getStatus()).body(response);
		}  catch (SubscriptionRequestException exception) {
			response = new ApiResponseDTO<>(HttpStatus.BAD_REQUEST, exception);
			return ResponseEntity.status(response.getStatus()).body(response);
		} catch (Exception exception) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	@ApiOperation(
			value = "Cancel a subscription",
			notes = "Cancels the subscription associated to the given Subscription ID",
			tags = {"Subscription"})
	@ApiResponses(
			value = {
					@ApiResponse(code = 200, message = "Subscription canceled", response = ApiResponseDTO.class),
					@ApiResponse(
							code = 404,
							message = "Returns ApiResponseDTO object with cause details, mainly the given Subscription ID is not associated with any known resource.",
							response = ApiResponseDTO.class)
			})
	@DeleteMapping(value = "/v1.0/subscriptions/{subscriptionId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponseDTO<String>> cancelSubscription(@PathVariable Long subscriptionId) {
		ApiResponseDTO<String> response;
		try {
			subscriptionService.cancelSubscription(subscriptionId);
			response = new ApiResponseDTO<>(HttpStatus.OK);

			return ResponseEntity.status(response.getStatus()).body(response);
		} catch (SubscriptionRequestException e) {
			response = new ApiResponseDTO<>(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
			return ResponseEntity.status(response.getStatus()).body(response);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ApiResponseDTO handleValidationExceptions(MethodArgumentNotValidException ex) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});

		return new ApiResponseDTO<Map>(HttpStatus.BAD_REQUEST, MethodArgumentNotValidException.class.getSimpleName(), errors);
	}
}
