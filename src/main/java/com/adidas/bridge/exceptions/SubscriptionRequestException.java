package com.adidas.bridge.exceptions;

import com.adidas.bridge.utils.constants.Messages;
import lombok.Getter;

import static java.lang.String.format;

@Getter
public class SubscriptionRequestException extends Exception {
	private static final String MESSAGE = Messages.SUBSCRIPTION_REQUEST_FAILED;
	private static final String ERROR = Messages.ERROR;

	private Object errorDetails;

	public SubscriptionRequestException() {
		super(MESSAGE);
	}

	public SubscriptionRequestException(String error) {
		super(MESSAGE.concat(format(ERROR, error)));
	}
	public SubscriptionRequestException(String error, Object errorDetails) {
		super(MESSAGE.concat(format(ERROR, error)));
		this.errorDetails = errorDetails;
	}

}
