package com.adidas.bridge.utils;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class HttpUtils {
	public static <T, K> K sendRequest(String apiUrl, T body, HttpMethod httpMethod, Class<K> type, RestTemplate restTemplate) {
		MultiValueMap<String, String> header = new LinkedMultiValueMap<>();
		header.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<Object> requestEntity = new HttpEntity<>(body, header);

		ResponseEntity<K> response;
		response = restTemplate.exchange(apiUrl, httpMethod, requestEntity, type);
		return response.getBody();
	}
}
