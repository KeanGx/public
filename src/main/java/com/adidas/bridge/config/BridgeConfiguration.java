package com.adidas.bridge.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.client.RestTemplate;

public class BridgeConfiguration {

	@Configuration
	@Profile({ "default" })
	@PropertySource("classpath:application.properties")
	static class CloudDefaultConfig {
		@LoadBalanced
		@Bean
		RestTemplate restTemplate() {
			return new RestTemplate();
		}
	}

	@Configuration
	@Profile({ "test" })
	@PropertySource("classpath:application.properties")
	static class CloudTestConfig {
		@Primary
		@Bean
		RestTemplate restTemplate() {
			return new RestTemplate();
		}
	}

}
