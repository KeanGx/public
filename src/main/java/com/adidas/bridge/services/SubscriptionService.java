package com.adidas.bridge.services;

import com.adidas.bridge.clients.SubscriptionClient;
import com.adidas.bridge.dtos.requests.SubscriptionRequest;
import com.adidas.bridge.dtos.responses.SubscriptionData;
import com.adidas.bridge.exceptions.SubscriptionRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Service
@Validated
public class SubscriptionService implements Serializable {

	@Autowired
	private SubscriptionClient subscriptionClient;

	public Long createSubscription(@NotNull @Valid SubscriptionRequest subscriptionRequest) throws Exception {
		return subscriptionClient.createSubscription(subscriptionRequest);
	}

	public SubscriptionData findSubscription(@NotNull Long subscriptionId) throws SubscriptionRequestException {
		return subscriptionClient.findById(subscriptionId);
	}

	public List<SubscriptionData> retrieveSubscriptions(List<Long> ids, List<String> emails, List<Long> newsletterIds) throws SubscriptionRequestException {
		return subscriptionClient.retrieveSubscriptions(ids, emails, newsletterIds);
	}

	public void cancelSubscription(@NotNull Long subscriptionId) throws SubscriptionRequestException {
		subscriptionClient.cancelSubscription(subscriptionId);
	}
}
