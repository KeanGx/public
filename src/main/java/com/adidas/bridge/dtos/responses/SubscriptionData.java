package com.adidas.bridge.dtos.responses;

import com.adidas.bridge.utils.enums.GenderTypes;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class SubscriptionData {

	@ApiModelProperty(required = true)
	private Long id;

	@ApiModelProperty(required = true)
	private String email;

	@ApiModelProperty(required = true)
	private Date birthdate;

	@ApiModelProperty(required = true)
	private boolean consent;

	@ApiModelProperty(required = true)
	private Long newsletterId;

	private String firstName;
	private GenderTypes gender;
}
