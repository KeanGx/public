package com.adidas.bridge.dtos.requests;

import com.adidas.bridge.utils.constraints.Enum;
import com.adidas.bridge.utils.enums.GenderTypes;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class SubscriptionRequest {

	@Email(message = "Email is required and follow the correct format of an email")
	@ApiModelProperty(required = true)
	private String email;

	@NotNull(message = "Date of Birth is required.")
	@ApiModelProperty(required = true)
	private Date birthdate;

	@NotNull(message = "Consent flag is required.")
	@ApiModelProperty(required = true)
	private Boolean consent;

	@NotNull(message = "Newsletter campaign ID is required.")
	@ApiModelProperty(required = true)
	private Long newsletterId;

	private String firstName;

	@Enum(enumClass = GenderTypes.class, message = "Must be a valid value of GenderTypes enum.")
	private String gender;
}
