package com.adidas.bridge.clients;

import com.adidas.bridge.dtos.requests.SubscriptionRequest;
import com.adidas.bridge.dtos.responses.ApiResponseDTO;
import com.adidas.bridge.dtos.responses.SubscriptionData;
import com.adidas.bridge.exceptions.SubscriptionRequestException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import java.util.List;
import java.util.stream.Collectors;

import static com.adidas.bridge.utils.HttpUtils.sendRequest;

@RibbonClient(name = "subscription")
@Component
@Validated
public class SubscriptionClient {

	@Autowired
	private RestTemplate restTemplate;

	@Value("${subscription.api.url:http://subscription/}")
	private String subscriptionClientUrl;

	private final Logger log = LoggerFactory.getLogger(SubscriptionClient.class);

	public Long createSubscription(@NotNull @Valid SubscriptionRequest subscriptionRequest) throws SubscriptionRequestException, JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();

		try {
			UriComponentsBuilder uri = UriComponentsBuilder.fromUriString(subscriptionClientUrl + "/v1.0/subscriptions");
			ApiResponseDTO<Long> apiResponseDTO = sendRequest(uri.toUriString(), subscriptionRequest, HttpMethod.POST, ApiResponseDTO.class, restTemplate);

			if(!apiResponseDTO.getStatus().equals(HttpStatus.CREATED)){
				throw new SubscriptionRequestException(apiResponseDTO.getError(), apiResponseDTO.getDetail());
			}

			return mapper.convertValue(apiResponseDTO.getData(), new TypeReference<Long>(){} );
		} catch (SubscriptionRequestException e) {
			log.error("Failed to create subscription. Error: {}", e.getLocalizedMessage());
			throw e;
		} catch (HttpClientErrorException e) {
			log.error("Failed to create subscription. Error: {}", e.getResponseBodyAsString());
			ApiResponseDTO apiResponseDTO = mapper.readValue(e.getResponseBodyAsString(), ApiResponseDTO.class);
			throw new SubscriptionRequestException(apiResponseDTO.getError(), apiResponseDTO.getDetail());
		} catch (Exception e) {
			log.error("Failed to create subscription. Error: {}", e.getLocalizedMessage());
			throw new SubscriptionRequestException(e.getLocalizedMessage());
		}
	}

	public SubscriptionData findById(@NotNull Long subscriptionId) throws SubscriptionRequestException {
		try {
			UriComponentsBuilder uri = UriComponentsBuilder.fromUriString(subscriptionClientUrl + "/v1.0/subscriptions/" + subscriptionId);
			ApiResponseDTO<SubscriptionData> apiResponseDTO = sendRequest(uri.toUriString(), null, HttpMethod.GET, ApiResponseDTO.class, restTemplate);

			if(!apiResponseDTO.getStatus().equals(HttpStatus.OK)){
				throw new SubscriptionRequestException(apiResponseDTO.getError(), apiResponseDTO.getDetail());
			}

			ObjectMapper mapper = new ObjectMapper();
			return mapper.convertValue(apiResponseDTO.getData(), new TypeReference<SubscriptionData>(){} );
		} catch (SubscriptionRequestException e) {
			log.error("Failed to find subscription by ID. Error: {}", e.getLocalizedMessage());
			throw e;
		} catch (Exception e) {
			log.error("Failed to find subscription by ID. Error: {}", e.getLocalizedMessage());
			throw new SubscriptionRequestException(e.getLocalizedMessage());
		}
	}

	public List<SubscriptionData> retrieveSubscriptions(List<Long> ids, List<String> emails, List<Long> newsletterIds) throws SubscriptionRequestException {
		try {
			UriComponentsBuilder uri = UriComponentsBuilder.fromUriString(subscriptionClientUrl + "/v1.0/subscriptions/");
			if(!CollectionUtils.isEmpty(ids)) {
				uri.queryParam(
						"ids",
						ids.stream().map(String::valueOf).collect(Collectors.joining(","))
				);
			}

			if(!CollectionUtils.isEmpty(emails)) {
				uri.queryParam(
						"emails",
						emails.stream().map(String::valueOf).collect(Collectors.joining(","))
				);
			}

			if(!CollectionUtils.isEmpty(newsletterIds)) {
				uri.queryParam(
						"newsletterIds",
						newsletterIds.stream().map(String::valueOf).collect(Collectors.joining(","))
				);
			}

			ApiResponseDTO<SubscriptionData> apiResponseDTO = sendRequest(uri.toUriString(), null, HttpMethod.GET, ApiResponseDTO.class, restTemplate);

			if(!apiResponseDTO.getStatus().equals(HttpStatus.OK)){
				throw new SubscriptionRequestException(apiResponseDTO.getError(), apiResponseDTO.getDetail());
			}

			ObjectMapper mapper = new ObjectMapper();
			return mapper.convertValue(apiResponseDTO.getData(), new TypeReference<List<SubscriptionData>>(){} );
		} catch (SubscriptionRequestException e) {
			log.error("Failed to retrieve Subscriptions list. Error: {}", e.getLocalizedMessage());
			throw e;
		} catch (Exception e) {
			log.error("Failed to retrieve Subscriptions list. Error: {}", e.getLocalizedMessage());
			throw new SubscriptionRequestException(e.getLocalizedMessage());
		}
	}

	public void cancelSubscription(Long subscriptionId) throws SubscriptionRequestException {
		try {
			UriComponentsBuilder uri = UriComponentsBuilder.fromUriString(subscriptionClientUrl + "/v1.0/subscriptions/" + subscriptionId);
			ApiResponseDTO<SubscriptionData> apiResponseDTO = sendRequest(uri.toUriString(), null, HttpMethod.DELETE, ApiResponseDTO.class, restTemplate);

			if(!apiResponseDTO.getStatus().equals(HttpStatus.OK)){
				throw new SubscriptionRequestException(apiResponseDTO.getError(), apiResponseDTO.getDetail());
			}
		} catch (SubscriptionRequestException e) {
			log.error("Failed to find subscription by ID. Error: {}", e.getLocalizedMessage());
			throw e;
		} catch (Exception e) {
			log.error("Failed to find subscription by ID. Error: {}", e.getLocalizedMessage());
			throw new SubscriptionRequestException(e.getLocalizedMessage());
		}
	}
}
