# Subscription Service

Public Service serves as a bridge between the Frontend UI and the Backend services.

## Clone the Repository
Use this link to clone the [repository](https://bitbucket.org/KeanGx/public/src/master/) to your local.

## Run the APP
For example, to run the app using IntelliJ you could import the proyect as a Module From Existing Sources after cloning the repository in your local.
The app has been built with Maven and SpringBoot.

After importing it in your IDE be sure to download all dependencies, and you should be able to run the app with spring/maven.

Sadly I have no experience with Docker to be able to hand off the app within a container ready to run.

## Use the APP
To Start and be able to see the Swagger Documentation created for the Service, go to this url http://localhost:8680/swagger-ui.html.
With this you'll be able to see what endpoints are available and how to use them.

#### In the CI Directory you'll find a few configuration files for deploying the microserrvice into a kubernetes cluster in an AWS EKS Cluster.
#### Also in the CI Directory you'll find a JenkinsFile with a possible CI/CD pipeline for the app.

## Main Dependecies

* spring-boot-starter-web: This is used to be able to create the request/endpoints of the App
* spring-cloud-starter-netflix-ribbon: This is used to create the ribbons to be able to connect the applications with each other.
* spring-boot-starter-test: This is used for JUnit
* spring-boot-starter-validation: This is used for all the business validation checks added.
* lombok: Just like the previous one, Lombok helps use reduce the boiler plate of the getters/setters/constructors of each object,
* springfox-swagger2/springfox-swagger-ui: Both of this are used to be able to show a documentation via Swagger.
* spring-cloud-starter-contract-stub-runner/spring-cloud-starter-contract-verifier: This could be used in the future to add Spring Cloud Contracts to the app and be able to rn integrations tests between the multiple microservices.




